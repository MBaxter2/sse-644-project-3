"""
audio_tools is a module for manipulating audio
"""
import numpy as np
import scipy.signal as sg
import scipy.fftpack as fp
import scipy.io.wavfile as siw

def load_audio(file):
    """Reads a WAV file from disk. Only the first audio channel is returned.
    The returned data is normalized to be between -1 and 1.

    Arguments:
        * file: Filepath to WAV file

    Returns:
        * rate: Recorded rate of audio
        * data: Normalized audio data
    """
    rate, data = siw.read(file)
    dtype = data.dtype
    scale = 2.**((8 * dtype.itemsize) - 1)
    if data.ndim > 1:
        output = data[:, 0] / scale
    else:
        output = data / scale
    return (rate, output)

def apply_butter_filter(data, rate, cutoff, high_low):
    """Apply a Butterworth low-pass or high-pass filter to the data.

    Arguments:
        * data: NumPy data array
        * rate: Recorded rate of audio
        * cutoff: Frequency cutoff of filter
        * high_low: Boolean to select high-pass (True) or low-pass (False)

    Returns:
        * filtered data
    """
    ftype = 'high' if high_low else 'low'
    num, denom = sg.butter(4, cutoff/(rate/2.), ftype)
    filtered = sg.filtfilt(num, denom, data)
    return filtered

def calc_freq(data, rate):
    """Calculate the frequency data of a digital signal

    Arguments:
        * data: NumPy data array
        * rate: Recorded rate of audio

    Returns:
        * decibel data
        * frequency
    """
    fft = fp.fft(data)
    psd = np.abs(fft) ** 2
    freq = fp.fftfreq(len(psd), 1./rate)
    i = freq > 0
    return freq[i], 10 * np.log10(psd[i])
